import Mirador from 'mirador/dist/es/src/index';
import { miradorImageToolsPlugin } from 'mirador-image-tools';
import LocalStorageAdapter from 'mirador-annotations/es/LocalStorageAdapter';
import annotationPlugins from 'mirador-annotations/es/';

let params = new URLSearchParams(window.location.search),
    config = {
        id: 'pia-mirador',
        annotation: {
            adapter: (canvasId) => new LocalStorageAdapter(`localStorage://?canvasId=${canvasId}`),
            exportLocalStorageAnnotations: false, // display annotation JSON export button
          },
          window: {
            sideBarOpenByDefault: true,
            defaultSideBarPanel: 'annotations',
          },
    };

if(params.has('manifest')) {
    config['windows'] = [{
        'loadedManifest': params.get('manifest')
    }]
}

Mirador.viewer(config, [
  ...miradorImageToolsPlugin,
  ...annotationPlugins
]);